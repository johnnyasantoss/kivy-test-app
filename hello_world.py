from kivy.app import App
from kivy.uix.button import Button


class HWApp(App):
    def build(self):
        self.title = "Hello world App"
        return Button(text="Hello World")


HWApp().run()
